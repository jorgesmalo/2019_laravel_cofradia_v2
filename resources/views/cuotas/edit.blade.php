@extends('layouts.layout_jorge')
@section('contenido')
	<a href="{{ url()->previous()  }}"><button type="button" class="btn btn-outline-dark">Volver</button></a>
<form method="post" action="{{ route('cuotas.update' , $cuota->id) }}" enctype="multipart/form-data">
	@method('PUT')
	@csrf
  <div class="form-group">
    <div class="form-group col-12">
      <label for="text">Nombre</label>
      <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre y apellidos" value="<?php echo $cuota->nombre?>">
    </div>
    <div class="form-group col-12">
      <label for="text">Cantidad</label>
      <input type="number" class="form-control" id="valor" name="valor" placeholder="Cantidad" value="<?php echo $cuota->valor?>">
    </div>
  	<div class="form-group col-12">
	    <label for="inputAddress">Concepto</label>
	    <input type="text" class="form-control" id="concepto" name="concepto" placeholder="Pago mes Diciembre" value="<?php echo $cuota->concepto?>">
  	</div>
  	<div class="form-group col-12">
	    <div class="form-group col-md-6">
	      <label for="inputCity">Fecha Cobro</label>
	      <input type="date" id="fecha_cobro" name="fecha_cobro" value="<?php echo $cuota->fecha_cobro?>">
	    </div>
	</div>
	  @if(!empty($cuota->rutaFichero))
		  <img src="{{ asset('fotos_cuotas/'.$cuota->rutaFichero) }}">
	  @else
		  <p class="col-md-4"><strong>Sin Imagen</strong></p>
	  @endif
  	<div class="form-group col-12">
	  <label for="inputCity">Subir Imagen</label>
	  <input type="file" class="form-control-file" name="imagen" id="imagen">

  	</div>
 </div>
<button type="submit" class="btn btn-primary">Actualizar Datos</button>
</form>
@stop
