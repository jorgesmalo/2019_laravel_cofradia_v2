@extends('layouts.layout_jorge')
@section('contenido')
  <a href="{{ url()->previous()  }}"><button type="button" class="btn btn-outline-dark">Volver</button></a>
<div class="container">
    <p class="col-md-4"><strong>Nombre:</strong> {{ $cuota->nombre }}</p>
    <p class="col-md-4"><strong>Cantidad:</strong> {{ $cuota->valor }}</p>
    <p class="col-md-4"><strong>Concepto:</strong> {{ $cuota->concepto }}</p>
    <p class="col-md-4"><strong>Fecha:</strong> {{ $cuota->fecha_cobro }}</p>
    @if(!empty($cuota->rutaFichero))
        <img src="{{ asset('fotos_cuotas/'.$cuota->rutaFichero) }}">
    @else
        <p class="col-md-4"><strong>Sin Imagen</strong></p>
    @endif
</div>
@stop
 