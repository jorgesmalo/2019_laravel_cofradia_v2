@extends('layouts.layout_jorge2')
@section('contenido')
	<a class="waves-effect waves-light btn" href="{{ url()->previous()  }}">Volver</a>
	<form class="cols 12" method="post" action="{{ route('cuotas.store') }}" enctype="multipart/form-data">
		@csrf
		<div class="row">
			<div class="input-field col s12">
				<i class="material-icons prefix">account_circle</i>
				<input id="icon_prefix" type="text" class="validate" id="nombre" name="nombre">
				<label for="icon_prefix">Nombre y Apellidos</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<i class="material-icons prefix">euro_symbol</i>
				<input id="euro_symbol" type="number" class="validate" id="valor" name="valor">
				<label for="euro_symbol">Cantidad</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<i class="material-icons prefix">description</i>
				<input id="description" type="text" class="validate" id="concepto" name="concepto">
				<label for="description">Concepto</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<i class="material-icons prefix">calendar_today</i>
				<input type="date" class="datepicker" id="fecha_cobro" name="fecha_cobro">
				<label for="calendar_today">Fecha de Cobro</label>
			</div>
		</div>
		<div class="row">
		<div class="file-field input-field">
			<div class="btn">
				<span>Subir Imagen</span>
				<input type="file" name="imagen" id="imagen">
			</div>
			<div class="file-path-wrapper">
				<input class="file-path validate" type="text">
			</div>
		</div>
		</div>
		<button class="btn waves-effect waves-light" type="submit" name="action">Guardar Datos
			<i class="material-icons right">save</i>
		</button>

	</form>
	</div>
@stop	
