<!DOCTYPE html>
<html lang="es">
<head>
	<title>Administración de Cuotas-Cofradía</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="uper">
		@if (session()->get('success'))
		<div class="alert alert-success">
			{{ session()->get('success')}}
		</div>
		@endif
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div><br />
		@endif
	</div>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <a class="navbar-brand" href="#">Inicio</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav">
		      <li class="nav-item active">
		        <a class="nav-link" href="#">Socios</a>
		      </li>
		      <li class="nav-item active">
		        <a class="nav-link" href="#">Cuotas</a>
		      </li>
		      <li class="nav-item active">
		        <a class="nav-link" href="#">Tesorería</a>
		      </li>
		      <li class="nav-item active">
		        <a class="nav-link" href="#">Inventario</a>
		      </li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Roberto</a>
				</li>
			</ul>
			  <ul class="nav navbar-nav  ml-auto">
				<li class="nav-item active">
					<a class="nav-link" href="{{ route ('cerrarSesion')  }}">Cerrar Sesión</a>
				</li>
			  </ul>
		  </div>
		</nav>

		@yield('contenido')

	</div>

</body>
</html>