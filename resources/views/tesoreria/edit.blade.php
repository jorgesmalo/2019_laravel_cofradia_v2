@extends('layouts.layout')

	@section('content')
@if ($errors->any())
	<div>
		@foreach($errors->all() as $error)
		{{ $error }}
		@endforeach
	</div>
@endif
	@csrf
<a class="btn btn-success" href="{{ route('tesoreria.index') }}">Volver</a>
<form method="post" action="{{ route('tesoreria.update' , $tesoreria->id) }}" enctype="multipart/form-data">
	@method('PUT')
	@csrf
	<div class="form-group col-4 offset-md-4">
	<label>Nombre</label>
	<br>
    <input type="text" name="nombre" id="nombre" class="form-control" value="{{ $tesoreria->nombre }}">
	<br>
	<br>
	<label>Telefono</label>
	<br>
	<input type="text" name="telefono" id="telefono" class="form-control" value="{{ $tesoreria->telefono }}">
	<br>
	<br>
	<label>Fecha</label>
	<br>
	<input type="text" name="fecha" id="fecha" class="form-control" value="{{ $tesoreria->fecha }}">
	<br>
	<br>
	<label>Descripcion</label>
	<br>
	<input type="text" name="description" id="description" class="form-control" value="{{ $tesoreria->description }}">
	<br>
	<br>
	<label>Precio</label>
	<br>
		<input type="text" name="precio" class="form-control" id="precio" value="{{ $tesoreria->precio }}">
	<br>
	<br>
        <?php if (!empty($tesoreria->fotoFactura)){ ?>
		<img src=" {{ asset('storage/fotos_factura/'.$tesoreria->fotoFactura) }}"/>
        <?php } ?>
	<label for="imagen">Imagen:</label>
	<br>
		<input type="file" class="form-control-file" name="imagen" id="imagen" >
	<br>
		<br>
	</div>
	<button type="submit">Guardar</button>
</form>
@endsection