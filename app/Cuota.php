<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuota extends Model
{
	public $timestamps = false;
    protected $fillable = [
    	'nombre',
    	'valor',
    	'concepto',
    	'fecha_cobro',
        'rutaFichero'
    ];
}
