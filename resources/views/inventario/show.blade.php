@extends('layout.layout')
@section('content')
	<div class="container">


		
						<p><mark>ID</mark>:{{$listadoInventario->id}}</p>
						<p><mark>CÓDIGO</mark>:{{$listadoInventario->codigo}}</p>
						<p><mark>NOMBRE</mark>:{{$listadoInventario->nombre}}</p>
						<p><mark>CANTIDAD</mark>:{{$listadoInventario->cantidad}}</p>
						<p><mark>PROVEEDOR</mark>:{{$listadoInventario->proveedor}}</p>
						<p><mark>PRECIO</mark>:{{$listadoInventario->precio}}€</p>
						<p><mark>DISPONIBILIDAD</mark>:{{$listadoInventario->disponibilidad}}</p>
						@if(!empty($listadoInventario->ruta))
							<p><mark>IMAGEN</mark>:<img src="{{ asset ('storage/'.$listadoInventario->ruta) }}"></p>
						@endif
				
		
	</div>
@endsection