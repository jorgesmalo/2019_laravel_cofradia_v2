-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-02-2019 a las 14:07:43
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laravel2019`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuotas`
--

CREATE TABLE `cuotas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` int(11) NOT NULL,
  `concepto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_cobro` date NOT NULL,
  `rutaFichero` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cuotas`
--

INSERT INTO `cuotas` (`id`, `nombre`, `valor`, `concepto`, `fecha_cobro`, `rutaFichero`) VALUES
(11, 'ghfgf', 55, 'dfss', '2019-02-22', 'cuota5c6ea26d795d32.81870405_10.jpg'),
(14, 'Pepito Grillo22', 22, 'sdadassdf', '2019-02-23', 'cuota5c6ff22b038064.82046225_2.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cantidad` mediumint(9) NOT NULL,
  `precio` double(8,2) NOT NULL,
  `disponibilidad` tinyint(1) DEFAULT NULL,
  `proveedor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ruta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`id`, `codigo`, `nombre`, `descripcion`, `cantidad`, `precio`, `disponibilidad`, `proveedor`, `ruta`, `created_at`, `updated_at`) VALUES
(1, '0001', 'baculo', 'asdasd', 2, 3.00, 0, 'asdasdad', '', NULL, NULL),
(3, '1231654', 'baculo', 'asdads', 2, 3.00, 0, 'asdasdad', '', NULL, '2019-02-01 11:38:50'),
(4, '5000', 'algo', 'asd', 5, 6.50, 1, 'asd', '', NULL, NULL),
(5, '123123132213', 'asdasd', 'asdasd', 3, 23.00, NULL, 'asdad', NULL, '2019-02-13 09:15:33', '2019-02-13 09:15:33'),
(6, '124124', 'asdfsadfafsa', 'asdfasdf', 3, 1.56, NULL, 'dasda', NULL, '2019-02-13 09:19:08', '2019-02-13 09:19:08'),
(7, '1231321', 'asdasdad', 'asdad', 12, 12.00, NULL, 'dsafsghdfghdfgh', '', '2019-02-13 09:20:42', '2019-02-13 09:20:42'),
(8, '25025213', 'asdasd', 'asda', 12, 12.00, NULL, 'dasda', '', '2019-02-14 11:14:01', '2019-02-14 11:14:01'),
(9, '222222', 'Imagen', 'asdads', 3, 12.00, NULL, 'algods', '', '2019-02-14 11:19:24', '2019-02-14 11:19:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_01_24_122814_crear_inventario_tabla', 1),
(7, '2019_01_24_122820_crear_socios_tabla', 1),
(8, '2019_01_24_122922_create_tesoreria_table', 1),
(9, '2019_01_24_125755_create_cuotas_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE `socios` (
  `id` int(10) UNSIGNED NOT NULL,
  `dni` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_de_nacimiento` date NOT NULL,
  `localidad` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Almansa',
  `provincia` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Albacete',
  `codigo_postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '02640',
  `direccion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'España',
  `baja` tinyint(1) DEFAULT NULL,
  `observaciones` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ruta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`id`, `dni`, `nombre`, `apellidos`, `telefono`, `fecha_de_nacimiento`, `localidad`, `provincia`, `codigo_postal`, `direccion`, `pais`, `baja`, `observaciones`, `ruta`, `created_at`, `updated_at`) VALUES
(13, 'j', 'hjbn', 'hj', 'j', '2019-02-08', 'j', 'j', 'j', 'j', 'j', NULL, 'j,', NULL, '2019-02-04 06:14:18', '2019-02-04 06:22:47'),
(14, 'dfg', 'sdfg', 'dfg', 'dfg', '2019-02-08', 'dfhg', 'dfg', 'dfg', 'dfg', 'dfg', NULL, 'dfg', NULL, '2019-02-06 07:28:45', '2019-02-06 07:28:45'),
(15, 'dfhg', 'dcfb', 'df', 'dfh', '2019-02-15', 'dfbh', 'dfbh', 'dfb', 'dfb', 'dfb', NULL, 'dfb', NULL, '2019-02-06 07:32:37', '2019-02-06 07:32:37'),
(16, '33333333333', 'Pepito Grillo', 'fdghfd', '33333333333', '2019-02-23', 'Almansa', 'Abacete', '445454', 'AAAAAAAA', 'AAAAAAAAAAAA', NULL, 'JEJEJ', 'img_5c6e8d4db3b582.17287878_1.jpg', '2019-02-21 10:36:45', '2019-02-21 10:36:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tesoreria`
--

CREATE TABLE `tesoreria` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `fecha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `precio` double(8,2) NOT NULL,
  `fotoFactura` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tesoreria`
--

INSERT INTO `tesoreria` (`id`, `nombre`, `telefono`, `fecha`, `description`, `precio`, `fotoFactura`) VALUES
(46, 'Jorge', 987547245, '2019-01-11', 'Compra de la bebida', 80.00, 'ruta'),
(47, 'dani', 52222, '2019-02-02', 'hola', 23.00, '12.jpg'),
(48, 'maria', 23456789, '2019-02-14', 'foto prueba', 23.00, 'cof_5c6562cbd7d9c3.29993874_1.jpg'),
(49, 'dani', 234566, '2019-02-07', 'prueba 2', 23.00, 'cof_5c65662d11c526.37293696_9.jpg'),
(50, 'Roberto', 655675342, '2019-02-05', 'Pago botas', 53.00, 'cof_5c659f7db75949.10214377_2.jpg'),
(51, 'manuel', 234556, '2019-02-06', 'no funciona nada', 23.00, 'cof_5c66acc2331405.64862730_php.net.txt');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'daw', 'daw@daw.es', NULL, '$2y$10$cuwFnm7msmgI4rHZn/d/4OGLwQQYV3GYKnFG2n9Y4OXGfrAEWwzla', NULL, '2019-02-21 10:17:02', '2019-02-21 10:17:02');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tesoreria`
--
ALTER TABLE `tesoreria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `socios`
--
ALTER TABLE `socios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `tesoreria`
--
ALTER TABLE `tesoreria`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
