@extends('layouts.layout_jorge')
@section('contenido')
<a href="cuotas/create/"><button type="button" class="btn btn btn-success">Añadir</button></a>
<a href="{{ url('dynamic_pdf/pdf') }}" class="btn btn-danger">Convertir a PDF</a>
<div class="table-responsive">
	<table class="table  table-hover table-striped">
		<thead class="thead-dark">
			<th>Nombre</th>
			<th>Cantidad</th>
			<th>Concepto</th>
			<th>Fecha</th>
			<th>Mostrar</th>
			<th>Editar</th>
			<th>Eliminar</th>
		</thead>
		<tbody>
			<?php foreach ($listadoCuotas as $x) { ?>
			<tr>
				<td> <?php echo $x->nombre;?></td>
				<td> <?php echo $x->valor;?></td>
				<td><?php echo $x->concepto;?></td>
				<td><?php echo $x->fecha_cobro;?></td>
				<td><a href="cuotas/show/<?php echo $x->id ?>"><button type="button" class="btn btn-primary">Mostrar</button></a></td>
				<td><a href="cuotas/edit/<?php echo $x->id ?>"><button type="button" class="btn btn-info">Editar</button></a></td>
				<td>
					<form id="formulario<?php echo $x->id ?>" method="post" action="{{ route('cuotas.destroy', $x->id) }}">
						@method('DELETE')
					@csrf
						<!-- Button trigger modal -->
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter<?php echo $x->id; ?>">
								Eliminar
							</button>
							<!-- Modal -->
							<div class="modal fade" id="exampleModalCenter<?php echo $x->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Confirmación Eliminar</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											¿Seguro que quiere eliminar el registro?
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
											<button type="button" class="btn btn-primary" onclick="document.getElementById('formulario<?php echo $x->id ?>').submit();">Borrar</button>
										</div>
									</div>
								</div>
							</div>
					</form>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
</div>
@stop
