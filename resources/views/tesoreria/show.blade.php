@extends('layouts.layout')

	@section('content')
	<br>
	<br>
	<div class="col-4 offset-md-4">
		<span class="texto1">Nombre:</span> {{ $tesoreria->nombre }}
		<br>
		<span class="texto1">Telefono :</span> {{ $tesoreria->telefono }}
		<br>
		<span class="texto1">Fecha :</span> {{ $tesoreria->fecha }}
		<br>
		<span class="texto1">Descripcion :</span> {{ $tesoreria->description }}
		<br>
		<span class="texto1">Precio :</span> {{ $tesoreria->precio }}
		<br>
		<span class="texto1">Imagen :</span> <?php if (!empty($tesoreria->fotoFactura)){ ?>
			<img src=" {{ asset('storage/fotos_factura/'.$tesoreria->fotoFactura) }}"/>
		<?php } ?>
	</div>
	<a class="btn btn-success" href="{{ route('tesoreria.index') }}"> Volver</a>
@endsection


