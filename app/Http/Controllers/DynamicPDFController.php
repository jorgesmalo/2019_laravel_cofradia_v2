<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use App\Cuota;

class DynamicPDFController extends Controller
{
    function index()
    {
        $listadoCuotas= DB::table('cuotas')->get();
        return view('cuotas.dynamic_pdf',['customer_data'=>$listadoCuotas]);
        //$customer_data = $this->get_customer_data();
        //return view('dynamic_pdf')->with('customer_data', $customer_data);
    }

    function get_customer_data()
    {
       $customer_data = DB::table('cuotas')
            ->get();
        return $customer_data;
    }

    function pdf()
    {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->convert_customer_data_to_html());
        return $pdf->stream();
    }

    function convert_customer_data_to_html()
    {
        $customer_data = $this->get_customer_data();
        $output = '
     <h3 align="center">Listado de Cuotas</h3>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
      <tr>
    <th style="border: 1px solid; padding:12px;" width="20%">Nombre</th>
    <th style="border: 1px solid; padding:12px;" width="15%">Valor</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Concepto</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Fecha Cobro</th>

   </tr>
     ';
        foreach($customer_data as $customer)
        {
            $output .= '
      <tr>
       <td style="border: 1px solid; padding:12px;">'.$customer->nombre.'</td>
       <td style="border: 1px solid; padding:12px;">'.$customer->valor.'</td>
       <td style="border: 1px solid; padding:12px;">'.$customer->concepto.'</td>
       <td style="border: 1px solid; padding:12px;">'.$customer->fecha_cobro.'</td>
      </tr>
      ';
        }
        $output .= '</table>';
        return $output;
    }
}
